console.log("Hello World");

/*
	1. Create variables to store to the following user details:

	-first name - String
	-last name - String
	-age - Number
	-hobbies - Array
	-work address - Object

		-The hobbies array should contain at least 3 hobbies as Strings.
		-The work address object should contain the following key-value pairs:

			houseNumber: <value>
			street: <value>
			city: <value>
			state: <value>

	Log the values of each variable to follow/mimic the output.

	Note:
		-Name your own variables but follow the conventions and best practice in naming variables.
		-You may add your own values but keep the variable names and values Safe For Work.
*/

	//Add your variables and console log for objective 1 here:

	let firstName = "First Name: Rico Domino";
	console.log(firstName);

	let lastName = "Last Name: Collina";
	console.log(lastName);

	let myAge = "Age: 28";
	console.log(myAge);

	let hobbies ="Hobbies:";
	console.log(hobbies);

	let myHobbieslist = ["Coding", "Reading books", "Watching Netflix", "Cooking"];
	console.log(myHobbieslist);

	let workAddress ="Work Address:";
	console.log(workAddress);

	let addressInfo = {
		houseNumber: "27",
		street: "Caliya",
		city: "Candelaria",
		isActive: true

	};
	console.log(addressInfo);




/*			
	2. Debugging Practice - Identify and implement the best practices of creating and using variables 
	   by avoiding errors and debugging the following codes:

			-Log the values of each variable to follow/mimic the output.
*/	


	let fullName = "My Fulle name is: Steve Rogers";
	console.log(fullName);

	let currentAge = 40;
	console.log("My current age is: " + currentAge);

	let myFriends = "My Friends are:"
	console.log(myFriends);
	let friends = ["Tony", "Bruce", "Thor", "Natasha", "Clint", "Nick"];
	console.log(friends);
	
	let myProfile = "My Full profile:";
	console.log(myProfile);
	let profile = {

		username: "captain_america",
		fullName1: "Steve Rogers",
		age: 40,
		isActive: false,

	};
	console.log(profile);

	let fullName2 = "Bucky Barnes";
	console.log("My bestfriend is: " + fullName2);

	let lastLocation = "Atlantic Ocean";
	lastLocation = "Arctic Ocean";
	console.log("I was found frozen in: " + lastLocation);

